## Excutar a aplicação com Docker e Docker Compose

- Para executar a aplicação com Docker e Docker Compose, execute os seguintes comandos na raiz do projeto:

```bash
    docker-compose build
    docker-compose up
```

Arquivos de configuração do Docker e Docker Compose:

- Web page: [Dockerfile](https://gitlab.com/aSTRonuun/eshoponweb-devops/-/blob/main/src/Web/Dockerfile)
- Web API: [Dockerfile](https://gitlab.com/aSTRonuun/eshoponweb-devops/-/blob/main/src/PublicApi/Dockerfile)
- Docker Compose + DB SQL Server Container: [Docker Compose](https://gitlab.com/aSTRonuun/eshoponweb-devops/-/blob/main/docker-compose.yml)

